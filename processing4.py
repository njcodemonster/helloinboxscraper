import imaplib
import email
import io
import pymongo
from pathlib import Path
from pymongo import MongoClient
from datetime import datetime
from bson.son import SON
import time
clientMongo = MongoClient()
db = clientMongo.custrec
filetoout = open('main_subs.csv','r')
contents = Path('main_subs.csv').read_text()
contents = contents.split('\n')
filetoout = open('planconsolidation_3.csv','a')
outermainarray =  [''  for x in range(18)]
count =0 
for k in contents:
    outermainarray =  [''  for x in range(18)]
    p=k.split(',')
    #if p[0] != "bolick.alyssa@gmail.com":
    #    continue
    pipeline =  [     {"$sort": { "ts":1}} , { "$match":{"Eid":{"$regex":".*"+p[0]+".*"} } },  { "$project":{"year":{"$year":"$ts" } ,"month":{"$month": "$ts" },"day":{"$dayOfMonth": "$ts" },"action":"$action" ,"to":"$to","from":"$from","Eid":"$Eid","body":"$body","upgradedto":"$upgradedto","Downgradeto":"$Downgradeto"} }  ]
    cplan = ''
    outermainarray[0]=p[0]
    oldactiondate= datetime.now
    month =0 
    oldmonth = 0 
    social = 0
    tren=0
    wand =0 
    dayold = 0
    subday =0
    actionday = 0
    gotsignup = 0
    for val in db.cust.aggregate(pipeline):
        print(val['action'])
        #print(val['year'])
        #print(val['month'])
        if val['year'] == 2017:
                if val['month'] > 8:
                    month = val['month']-8
        else:
            month = val['month']+4
        if val['action'] == 'Ordered':
            if dayold == 1:
                dd = datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
                dif = dd - oldactiondate 
                print(dif.days)
                if cplan == 'Socialite':
                    social = social+dif.days
                if cplan == 'Trendsetter':
                    tren = tren+dif.days
                if cplan == 'Wanderlust':
                    wand = wand+dif.days
            oldactiondate =  datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')

        if val['action'] == 'Paused':
            if dayold == 1:
                dd = datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
                dif = dd - oldactiondate 
                print(dif.days)
                if cplan == 'Socialite':
                    social = social+dif.days
                if cplan == 'Trendsetter':
                    tren = tren+dif.days
                if cplan == 'Wanderlust':
                    wand = wand+dif.days
                cplan = ''
            oldactiondate =  datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
            
        if val['action'] == 'fpsignup':
            if dayold == 1:
                dd = datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
                dif = dd - oldactiondate 
                print(dif.days)
            oldactiondate =  datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
            dayold = 1
            if cplan == 'Socialite':
                social = social+dif.days
            if cplan == 'Trendsetter':
                tren = tren+dif.days
            if cplan == 'Wanderlust':
                wand = wand+dif.days
            cplan = val['body']
        if val['action'] == 'upgrade':
            if dayold == 1:
                dd = datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
                dif = dd - oldactiondate 
                print(dif.days)
            oldactiondate =  datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
            if cplan == 'Socialite':
                social = social+dif.days
            if cplan == 'Trendsetter':
                tren = tren+dif.days
            if cplan == 'Wanderlust':
                wand = wand+dif.days
            cplan = val['upgradedto']
        if val['action'] == 'Downgrade':
            if dayold == 1:
                dd = datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
                dif = dd - oldactiondate 
                print(dif.days)
            oldactiondate =  datetime.strptime(str(val['year'])+'-'+str(val['month'])+'-'+str(val['day']), '%Y-%m-%d')
            if cplan == 'Socialite':
                social = social+dif.days
            if cplan == 'Trendsetter':
                tren = tren+dif.days
            if cplan == 'Wanderlust':
                wand = wand+dif.days
            cplan = val['Downgradeto']
        



        if val['action'] == 'Downgrade' or val['action'] == 'upgrade' or val['action'] == 'fpsignup':
                oldmonth = month
                outermainarray[month] = outermainarray[month]+'#'+cplan
    if cplan != '':
        if cplan == 'Socialite':
            social = social+30
        if cplan == 'Trendsetter':
            tren = tren+30
        if cplan == 'Wanderlust':
            wand = wand+30
    count= count + 1
    outermainarray[14]=str(social/30)
    outermainarray[15]=str(tren/30)
    outermainarray[16]=str(wand/30)
    outermainarray[17]=str(count)
    print(outermainarray)
    filetoout.writelines(','.join(outermainarray)+"\n")