import imaplib
import email
import io
import pymongo
from pathlib import Path
from pymongo import MongoClient
from datetime import datetime
from bson.son import SON
import time


def timecount(s,e):
    print(str(s)+"##"+str(e))
    
    ds = 0
    de = 0
    ret = ''
    if s == e:
        ds = 0
        de = 30
    if s > e and s!= 31:
     #   print("in1")
        ds = 30 - s
        ds = ds + e
        de = s-e
    if s<e :
    #    print("in2")
        ds = e-s
        if e == 31:
            e=30
        de = 30 - e + s
        if de == 0:
            de = 1
    ret = str(ds)+"--"+str(de)
   # print(ret)
    return ret

#def timecountother(s,e,f):







clientMongo = MongoClient()
db = clientMongo.custrec
filetoout = open('main_subs.csv','r')
contents = Path('main_subs.csv').read_text()
contents = contents.split('\n')
filetoout = open('planconsolidation_4.csv','a')
outermainarray =  ['0'  for x in range(4)]
count =0 
for k in contents:
    outermainarray =  ['0'  for x in range(4)]
    p=k.split(',')
    #if p[0] != "jenlynnjoyce@gmail.com":
    #    continue
    pipeline =  [     {"$sort": { "ts":1}} , { "$match":{"Eid":{"$regex":".*"+p[0]+".*"} } },  { "$project":{"year":{"$year":"$ts" } ,"month":{"$month": "$ts" },"day":{"$dayOfMonth": "$ts" },"action":"$action" ,"to":"$to","from":"$from","Eid":"$Eid","body":"$body","upgradedto":"$upgradedto","Downgradeto":"$Downgradeto"} }  ]
    cplan = ''
    outermainarray[0]=p[0]
    month =0 
    oldmonth = 0 
    social = 0
    tren=0
    wand =0 
    dayold = 0
    subday =1
    gotsignup = 0
    for val in db.cust.aggregate(pipeline):
        #if gotsignup == 0 and val['action'] != 'fpsignup':
        #    continue
        #print(val['action'])
        #print(cplan)
        #print(val['year'])
        #print(val['month'])
        if val['action'] == 'Ordered' and cplan != '':
            if cplan == 'Socialite':
                outermainarray[1] = str(int(outermainarray[1])+1)
            if cplan == 'Trendsetter':
                outermainarray[2] = str(int(outermainarray[2])+1)
            if cplan == 'Wanderlust':
                outermainarray[3] = str(int(outermainarray[3])+1)
        if val['action'] == 'fpsignup':
           
            cplan = val['body']
            
        if val['action'] == 'upgrade':
            
           
            cplan = val['upgradedto']
            
        if val['action'] == 'Downgrade':
            cplan = val['Downgradeto']
    print(outermainarray)
    filetoout.writelines(','.join(outermainarray)+"\n")