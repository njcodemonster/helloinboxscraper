import imaplib
import email
import io
import pymongo
from pathlib import Path
from pymongo import MongoClient
from datetime import datetime
from bson.son import SON
import time


def timecount(s,e):
    print(str(s)+"##"+str(e))
    
    ds = 0
    de = 0
    ret = ''
    if s == e:
        ds = 0
        de = 30
    if s > e and s!= 31:
     #   print("in1")
        ds = 30 - s
        ds = ds + e
        de = s-e
    if s<e :
    #    print("in2")
        ds = e-s
        if e == 31:
            e=30
        de = 30 - e + s
        if de == 0:
            de = 1
    ret = str(ds)+"--"+str(de)
   # print(ret)
    return ret


def timecountfordown(s,e):
    print(str(s)+"##"+str(e))
    
    ds = 0
    de = 0
    ret = ''
    if s == e:
        ds = 0
        de = 30
    if s > e and s!= 31:
     #   print("in1")
        ds = 30 - s
        ds = ds + e
        de = s-e
    if s<e :
    #    print("in2")
        ds = e-s
        if e == 31:
            e=30
        de = 30 - e + s
        if de == 0:
            de = 1
    ret = str(ds)+"--"+str(de)
   # print(ret)
    return ds
#def timecountother(s,e,f):







clientMongo = MongoClient()
db = clientMongo.custrec
filetoout = open('main_subs.csv','r')
contents = Path('main_subs.csv').read_text()
contents = contents.split('\n')
filetoout = open('planconsolidation_3.csv','a')
outermainarray =  [''  for x in range(18)]
count =0 
for k in contents:
    outermainarray =  [''  for x in range(18)]
    p=k.split(',')
    #if p[0] != "albpalmer31@gmail.com":
    #    continue
    pipeline =  [     {"$sort": { "ts":1}} , { "$match":{"Eid":{"$regex":".*"+p[0]+".*"} } },  { "$project":{"year":{"$year":"$ts" } ,"month":{"$month": "$ts" },"day":{"$dayOfMonth": "$ts" },"action":"$action" ,"to":"$to","from":"$from","Eid":"$Eid","body":"$body","upgradedto":"$upgradedto","Downgradeto":"$Downgradeto"} }  ]
    cplan = ''
    outermainarray[0]=p[0]
    month =0 
    oldmonth = 0 
    social = 0
    tren=0
    wand =0 
    dayold = 0
    subday =1
    actionday = 0
    gotsignup = 0
    for val in db.cust.aggregate(pipeline):
        #if gotsignup == 0 and val['action'] != 'fpsignup':
        #    continue
        #print(val['action'])
        #print(val['year'])
        #print(val['month'])
        if val['year'] == 2017:
                if val['month'] > 8:
                    month = val['month']-8
        else:
            month = val['month']+4
        if val['action'] != 'Paused':
            if month > oldmonth:
                for x in range(oldmonth,month):
                    if x != 0:
                        if '--' in cplan:
                            cplan = cplan.split("--")[0]
                        #if outermainarray[x] == '':
                            #print("monthputting:"+str(x))
                            #outermainarray[x] = cplan
                            #if cplan == 'Socialite':
                            #    social = social+1
                            #if cplan == 'Trendsetter':
                            #    print('a1')
                            #    tren = tren+1
                            #if cplan == 'Wanderlust':
                            #    wand = wand+1
                        #else:
                            #print('month not putting'+str(x))
            if month != oldmonth:
                dayold = 0
        if val['action'] == 'fpsignup':
            #print('are we here')
            gotsignup = 1
            cplan = val['body']
            subday = val['day']
            actionday = subday
            dayold = 0
            if cplan == 'Socialite':
                social = social+1
            if cplan == 'Trendsetter':
                tren = tren+1
            if cplan == 'Wanderlust':
                wand = wand+1
        if val['action'] == 'upgrade':
            
            cplan_old = cplan
            cplanx = val['upgradedto']
            time_val = timecount(subday,val['day'])
            cplan = cplanx
            dayold = val['day']
            actionday = dayold
            time_val = time_val.split('--')
            
            if cplanx == 'Socialite':
                social = social+(int(time_val[1])/30)
                
            if cplanx == 'Trendsetter':
                tren = tren+(int(time_val[1])/30)
                print('a2'+str(tren)+"-"+str(month))
            if cplanx == "Wanderlust":
                #print('wand cond:'+str(val['upgradedto']))
                wand = wand+(int(time_val[1])/30)
            
            if cplan_old == 'Socialite':
                social = social+(int(time_val[0])/30)-1
            
            if cplan_old == 'Trendsetter':
                tren = tren+(int(time_val[0])/30)-1
            
            if cplan_old == 'Wanderlust':
                wand = wand+(int(time_val[0])/30)-1
            
        if val['action'] == 'Downgrade':
            cpo = cplan
            cplan = val['Downgradeto']
            dayold = 0
            if month > oldmonth and outermainarray[month] == '' and timecountfordown(actionday,val['day'])>28:
                if cpo == 'Socialite':
                    social = social+1
                if cpo == 'Trendsetter':
                    tren = tren+1
                    print('a3'+"-"+str(month))
                if cpo == 'Wanderlust':
                    wand = wand+1
            else:
                if cplan == 'Socialite':
                    social = social+1
                if cplan == 'Trendsetter':
                    tren = tren+1
                if cplan == 'Wanderlust':
                    wand = wand+1
            actionday = val['day']


        #if val['action'] == 'Paused':
           
         #   if month > oldmonth  and timecountfordown(actionday,val['day'])>28:
         #       if cplan == 'Socialite':
         #           social = social+1
         #       if cplan == 'Trendsetter':
         #           tren = tren+1
         #       if cplan == 'Wanderlust':
         #           wand = wand+1


        if val['action'] == 'Downgrade' or val['action'] == 'upgrade' or val['action'] == 'fpsignup':
                oldmonth = month
                outermainarray[month] = outermainarray[month]+'#'+cplan
                #    try:
                #        if p[val['month']-8+1] == '1':
                #            outermainarray[val['month']-8+1] = outermainarray[val['month']-8]
                #    except:
                #        print('errr')
    
    #print(count)
    count= count + 1
    outcount = 0
    for valOut in outermainarray:
        if outcount > 12 :
            continue
        #print(valOut+"--"+p[outcount])
        if valOut == '' and outcount != 0 and p[outcount] == '1':
            outermainarray[outcount]= cplan
            if cplan == 'Socialite':
                social = social+1
            if cplan == 'Trendsetter':
                tren = tren+1
            if cplan == 'Wanderlust':
                wand = wand+1
        outcount = outcount+1
    outermainarray[14]=str(social)
    outermainarray[15]=str(tren)
    outermainarray[16]=str(wand)
    outermainarray[17]=str(count)
    print(outermainarray)
    filetoout.writelines(','.join(outermainarray)+"\n")